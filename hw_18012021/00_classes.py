#!/usr/bin/env python3

rooms = ["living room", "bathroom", "kitchen", "garden"]
transport = { "animal" : "4 legs", "robot" : "wheels", "human" : "car"}
import err_print
class Err:

    def __init__(self, msg=""):
        self.msg = msg
        err_print.main(msg) if msg else err_print.main()

class Robot:

    a = {"SOS" : "robocop", "Maid" : "iRobot"}

    def __init__(self, robot):
        self.robot = robot
        self.job = Robot.a[robot]

class Animal:

    a,b,c = "dog", "cat", "pig"
    sounds = {a: "Woof", b: "Miaw", c: "Hoink Hoink"}

    def __init__(self,animal):
        a,b,c = Animal.a, Animal.b, Animal.c
        self.animal = animal
    def make_noise(self):
        sound = Animal.sounds[self.animal]
        return sound

    
class Human:
    def __init__(self):
        
        self.location = rooms

    def dog_sound(self):
        a = Animal("dog")
        print(a.make_noise())
        self.need_robot("garden","SOS")
    def pig_sound(self):
        a = Animal("pig")
        print(a.make_noise())
        self.need_robot("bathroom","Maid")
    def need_robot(self, *args):
        location = self.location
        robot,chck,loc = False, False, False
        robo_val = ["Maid", "SOS"]
        for i in args:
            if i in robo_val:
                robot, job = Robot(i), i
        for i in args:
            if i in location:
                loc = i
        if robot and loc: chck = True
        print(f"{robot.robot} to the {loc}! I need {robot.job}") if chck else Err()



a = Human()
a.dog_sound()
a.pig_sound()
a.need_robot("MISSSSS")

