#!/usr/bin/env python3


mx = 46

def main(err_msg = None):
    if err_msg is None:
        err_msg = "Unknown error"

    dmsg_len = err_msg.count('\n')
    if dmsg_len >= 1:
        m_line = True
        dmsg = n_line_def(err_msg,m_line)
    else:
        m_line = False
        dmsg = one_line_def(err_msg,m_line)

    dmsg = deco_msg(dmsg,m_line)
    prt_mid = "│                                                    │"
    print("┌────────────────────────────────────────────────────┐")
    print(prt_mid)
    if m_line:
        x = 0
        while x < len(dmsg):
            print(dmsg[x])
            x += 1
    else:
        print(dmsg)
    print(prt_mid)
    print("└────────────────────────────────────────────────────┘")


def find_spaces(msg):
    spcs = "  "
    indx_strt = msg.index(spcs,0,-1)
    if indx_strt > 2:
        count = mx - indx_strt
        splt_count = count / 2
        str_str = str(splt_count)
        if str_str[-1] == "0":
            splt_count = int(str_str[:-2]) / 2

            if  splt_count  % 2 == 0:
                strt = int(indx_strt)
                stp = int(splt_count)
                msg = msg[strt:strt + stp + 1] + msg[:strt] + msg[strt + stp:-1]
            else:
                splt_count = ( int(str_str[:-2]) + 1 ) / 2
                if splt_count  % 2 == 0:
                    strt = int(indx_strt)
                    stp = int(splt_count)
                    msg = msg[strt:strt + stp + 1] + msg[:strt] + msg[strt + stp:-1]
        else:
            splt_count = ( int(str_str[:-2]) + 1 ) / 2
            if splt_count  % 2 == 0:
                strt = int(indx_strt)
                stp = int(splt_count)
                msg = msg[strt:strt + stp + 1] + msg[:strt] + msg[strt + stp:-1]
            else:
                splt_count = int(str_str[:-2])
                splt_count = splt_count / 2
                if splt_count  % 2 == 0:
                    strt = int(indx_strt)
                    stp = int(splt_count)
                    msg = msg[strt:strt + stp + 1] + msg[:strt] + msg[strt + stp:-1]
    return msg


def deco_msg(dmsg,m_line):
    spcr_l = "│   "
    spcr_r = "   │"
    if m_line:
        x = 0
        while x < len(dmsg):
            msg = dmsg[x]
            len_msg = len(msg)
            if len_msg < mx:
                mysum = mx - len_msg
                msg = msg + " " * mysum
                msg = find_spaces(msg)
            y = spcr_l + msg + spcr_r
            if len(y) > 10:
                dmsg[x] = y
            else:
                dmsg.pop(x)
            x += 1
    else:
        if len(dmsg) < mx:
            mysum = mx - len(dmsg)
            dmsg = dmsg + " " * mysum
            dmsg = find_spaces(dmsg)
        dmsg = spcr_l + dmsg + spcr_r
    
    return dmsg


def n_line_def(err_msg,m_line):
    dmsg = err_msg.split('\n')
    dmsg_len = err_msg.count('\n')
    x = 0
    while x < dmsg_len:
        msg = dmsg[x]
        lngth = len(msg)
        if lngth > mx:
            msg_next = msg[mx:]
            msg = msg[:mx]
            dmsg[x] = msg
            dmsg.insert(x + 1, msg_next)
        else:
            dmsg[x] = msg
        x += 1
    
    return dmsg


def one_line_def(err_msg,m_line):
    msg = err_msg
    lngth = len(msg)
    x = 0
    dmsg = []
    if lngth > mx:
        msg_next = msg[mx:]
        msg = msg[:mx]
        dmsg.append(msg)
        dmsg.append(msg_next)
        m_line = True
    else:
        dmsg = msg

    return dmsg


#main(err_msg)
