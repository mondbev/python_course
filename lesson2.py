#!/usr/bin/env python3


# going to use this function a few times

def read_number_stubbornly(prompt):
    while True:
         try:
            return int(input(prompt))
         except ValueError:
             print("type a number mate!")



# -----------------------------------------------------------
# a program that receive 10 n from user and print the largest
# -----------------------------------------------------------

count = 1
holder_string = ""
while count <= 10:
     prompt_string = f"Give the {count} int: "
     x = read_number_stubbornly(prompt_string)
     holder_string += str(x)
     count += 1
print(f"Larget is... {max(holder_string)}")



# ---------------------------------------------------------
# ask for age in years, print in months. except value error
# ---------------------------------------------------------

age=read_number_stubbornly("age in years?")
print(f"Age in years: {age*12}")



# ----------------------------------------------------------------------
# ask from a user to type lines ; break on empty line and print reversed
# ----------------------------------------------------------------------

holder_string = ""
while True:
    line = input("Please insert a line: ")
    if len(line) == 0:
        break
    holder_string += line
print(holder_string[::-1])



# -------------------------------------------------------------------
# rand ints 1 - 1,000,000 and print int that divisible by 7 13 and 15
# -------------------------------------------------------------------

import random
while True:
    num = randint(1,1000000)
    if num % 15 == 0 and num % 13 == 0 and num % 7 == 0:
        break
print(num)



# -------------------------------------------------------------
# random 2 ints 1 - 10 , calculate their smallest multiplication
# -------------------------------------------------------------

from random import randint

num_1,num_2 = randint(1,5),randint(6,10) # random nums.
count = range(1,11) # get us 1 - 10 numbers to multiply

breaker = False # if we find a match it'll will be a loop in a loop
		# This way we can break the outer loop

for p in count: # p as in I love u p2 than u love me

	x = num_1 * p # multiply num_1 and assign it variable x for testing

        for p in count:
		y = num_2 * p

		if x == y: # now we can break the first match as it'll be the smallest
			print(f"Ah! {x} = {y}")
			breaker = True # assign our breaker
			break # break inner loop

	if breaker: # check if breaker condition True

		break # break outer loop and entire function



# ---------------------------------------------------------------------------------
# user guess 1 - 100 ; I print cold or warm and the bonus: fool him once in a while
# ---------------------------------------------------------------------------------

from random import randint

# First function to launch game
def start_guess():

    print("END game by typing the safe num: 666 \n") # Give user option to quit

    random_number = randint(1,100) # Our random number generated once

    count_games = 1 # Count num of games played

    # Start the game
    print(random_number)
    take_a_guess(random_number,count_games)


def take_a_guess(random_number,count_games):

    user_input = read_number_stubbornly("Give int: ") # Takes user's input and make sure is a num

    count_games += 1 # add + 1 to num of games played

    bomba = randint(1,10) # Add bomba fool, we want to fool the user every once in a while

    bomba_modulus = randint(2,5) # Random the Bomba modulus

    small_msg,big_msg = "Too small","Too big" # Define small/big messages


    # Determine if user want's to quit

    if user_input == 666:

        print("BYE BYE loser")
        return

    # User input SMALLER

    if user_input < random_number:

        if bomba % bomba_modulus == 0:

            print(big_msg) # Bomba fool msg - print LARGER msg

        else:

            print(small_msg) # Real msg

        take_a_guess(random_number,count_games) # Try again


    # User input LARGER

    elif user_input > random_number:

        if bomba % bomba_modulus == 0:

            print(small_msg) # Bomba fool msg - print SMALLER msg

        else:

            print(big_msg) # Real msg

        take_a_guess(random_number,count_games) # Try again


    # He won! start the podium event

    elif user_input == random_number:

        print(f"{user_input} = {random_number} BYE BYE you won")

        return

# Don't remove! let's play
start_guess()
