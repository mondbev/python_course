#!/usr/bin/env python3
 
# ---------------------------------
# get 2 strings from user for name and pass and compare with
# saved db . print Welcome Master or if false - INTRUDER ALERT
# ---------------------------------

dict_logon = { 'apple' : 'red', 'lettuce' : 'green', 'lemon' : 'yellow', 'orange' : 'orange' }
input_name = input("Name: ")

if len(input_name) > 0 and type(input_name) == str:

    if input_name in dict_logon:

        print(f"Hi {input_name}")

        input_pass = input("Pass: ")

        if input_pass == dict_logon.get(input_name):

            print("Welcome Master")

        else:

            print("INTRUDER ALERT")

    else:

        print("INTRUDER ALERT")

else:

    print("Wrong input")


# ---------------------------------
# get list of 20 sys.argv from CLI
# print all above average
# 99 90 15 28 38 44 50 81 79 60 99 90 15 28 38 44 50 81 79 60
# ---------------------------------

import sys

list_scores = sys.argv

# remove first arg as it's script name
list_scores.pop(0)

# turn list types str to int
count = 0

while count < len(list_scores):

    list_scores[count] = int(list_scores)

    count += 1

# get average
avg = int( sum(list_scores) / len(list_scores) )

# create new list based on score higher than avg
count = 0
list_scores_above_avg = []
while count < len(list_scores):
    if list_scores[count] > avg:
        list_scores_above_avg.append(list_scores[count])
    count += 1

# print scores above average
print(list_scores_above_avg)


# ---------------------------------
# get sys.argv including hosts names
# scan hosts file for the required hosts 
# and print their IP's
# ---------------------------------

import sys

# get the sys.argv to new list and remove prog name -> [0]
list_argv = sys.argv
list_argv.pop(0)

# open hosts file located in the same repo
file_hosts = open("hosts", "r")

# split known structure lines on newline 
list_file = file_hosts.read().split("\n")
file_hosts.close()

# remove empty last newline
del list_file[-1]

# split on known structure to get --> Host (left split) = (right split) IP
# and push those values to a proper dict
dict_hosts = {}

for splitter in list_file:

    splitter_left = splitter.split(" = ")[0]
    
    splitter_right = splitter.split(" = ")[1]
    
    dict_hosts[splitter_left] = splitter_right

# now we ready to check if the user argv is on our dict keys
count = 0

while count < len(list_argv):

    if list_argv[count] in dict_hosts.keys():
    
        print(f" Host: { list_argv[count] } IP: { dict_hosts.get( list_argv[count] ) }")

    else:
        
        print(f" Error, { list_argv[count] } not found ")
    
    count += 1


# ---------------------------------
#
# a program to identify anagrams in a list
#
# ---------------------------------

# add requested words from HW to a list
words = ["add", "dad", "help", "more", "rome"]

# start manipulating the list in a loop
i = 0
while i < len(words):
    # for each word in the list start comparison
    for word in words:
        # compare words based on length and ommit checking the same word twice
        if len(words[i]) == len(word) and words[i] != word:
            # 1 ) set counter to match character's found vs len(word)
            counter = 0
            # read characters in every word
            for x in word:
                # 1.1 ) if we found the same character count it
                if words[i].count(x):
                    counter += 1
            # 1.2 ) if the counter equal to len(word) that's a match
            if counter == len(words[i]) and i % 2 != 0:
                # ommit checking the next index as list structure is known
                # update index with our pair of anagrams
                words[i] = str(word) + " " + str(words[i])
                # pop the known index next to it
                words.pop(i - 1)

    i += 1

# results:
for word in words:

    print(word)
