#!/usr/env/bin python3

# --------
#     1
# -----------

rect_print = """
The rectangle properties are:
Length: 2 centimeter.
Width: 3 centimeter.
"""
print(rect_print, end=" ")


# --------
#     2
# -----------

student = {
    "Name:" : "Ran Chen",
    "Address:" : "Habanim 35 Herzlia",
    "Telephone:" : "050-8754522",
    "Age:" : 24,
    "Faculty:" : "Computer Science",
    "Institution:" : "Mla",
    "Average:" : 78.5
 }
for key in student:
    print(f"{key} : {student[key]}") 


# --------
#     3
# -----------

print("AB\nABC\nABCD")


# --------
#     4
# -----------


